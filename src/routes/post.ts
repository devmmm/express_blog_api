import { Router } from "express";
import {
  createpost,
  allposts,
  editpost,
  deletepost,
  getpost
} from "../controllers/post";
import { body } from "express-validator";
import isAuth from "../middlewares/isAuth";
const router = Router();

router.get("/allposts", [], allposts);
router.get("/post/:id", [], getpost);
router.post(
  "/createpost",
  isAuth,
  [
    body("title")
      .notEmpty()
      .withMessage("Required post title"),
    body("description")
      .notEmpty()
      .withMessage("Required post description"),
    body("category")
      .notEmpty()
      .withMessage("Category Id required.")
  ],
  createpost
);
router.put(
  "/editpost",
  isAuth,
  [
    body("title")
      .notEmpty()
      .withMessage("Required post title"),
    body("description")
      .notEmpty()
      .withMessage("Required post description"),
    body("category")
      .notEmpty()
      .withMessage("Category Id required."),
    body("id")
      .notEmpty()
      .withMessage("Post Id required.")
  ],
  editpost
);
router.delete(
  "/deletepost",
  isAuth,
  [
    body("id")
      .notEmpty()
      .withMessage("Post Id required.")
  ],
  deletepost
);
export default router;
