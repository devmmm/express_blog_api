import {
  getallcategory,
  categorydetail,
  editcategory,
  deletecategory,
  CreateCategory
} from "../controllers/category";
import { Router } from "express";
import { body } from "express-validator";
import isAuth from "../middlewares/isAuth";
const router = Router();
import Category from "../models/cateogory";
router.get("/allcategories", [], getallcategory);
router.get("/category_detail/:id", categorydetail);
router.post(
  "/createCategory",
  isAuth,
  [
    body("name")
      .notEmpty()
      .withMessage("Category name required")
  ],
  CreateCategory
);
router.put(
  "/editcategory",
  isAuth,
  [
    body("name")
      .notEmpty()
      .withMessage("Category name required"),
    body("id")
      .custom(async (v: any) => {
        return Category.findOne({ name: v }).then(category => {
          if (category) {
            Promise.reject("This category name is already exist!");
          } else {
            Promise.resolve();
          }
        });
      })
      .notEmpty()
      .withMessage("Category Id required")
  ],

  editcategory
);
router.delete(
  "/deletecategory",
  isAuth,
  [
    body("id")
      .notEmpty()
      .withMessage("Category Id required")
  ],
  deletecategory
);

export default router;
