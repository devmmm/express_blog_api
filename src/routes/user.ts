import { login, signup, Editprofile, edit_avatar } from "../controllers/user";
import { Router } from "express";
import { body } from "express-validator";
import User from "../models/user";
import isAuth from "../middlewares/isAuth";

const router = Router();

router.post(
  "/signin",
  [
    body("email")
      .notEmpty()
      .isEmail()
      .trim()
      .normalizeEmail()
      .custom(async (val: any) => {
        return User.findOne({ email: val }).then(user => {
          if (!user) {
            Promise.reject("User does not exist!");
          } else {
            Promise.resolve();
          }
        });
      }),
    body("password")
      .notEmpty()
      .withMessage("Password must not be empty!")
  ],
  login
);

router.post(
  "/signup",
  [
    body("email")
      .notEmpty()
      .isEmail()
      .trim()
      .normalizeEmail()
      .custom(async (val: any) => {
        return User.findOne({ email: val }).then(user => {
          if (!user) {
            Promise.resolve();
          } else {
            Promise.reject("User Email already used!");
          }
        });
      }),
    body("password")
      .notEmpty()
      .withMessage("Password must not be empty!")
  ],
  signup
);

router.put(
  "/editprofile",
  isAuth,
  [
    body("fullname")
      .notEmpty()
      .withMessage("Full Name must not be empty"),
    body("address")
      .notEmpty()
      .withMessage("Address must not be empty"),
    body("city")
      .notEmpty()
      .withMessage("City must not be empty"),
    body("country")
      .notEmpty()
      .withMessage("Country must not be empty")
  ],
  Editprofile
);
router.put("/editavatar", isAuth, [], edit_avatar);
export default router;
