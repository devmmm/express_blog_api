import { model, Schema } from "mongoose";
import isEmail from "validator/lib/isEmail";
import bcrypt from "bcrypt";
const userSchema = new Schema(
  {
    avatar: {
      type: String,
      default: ""
    },
    email: {
      type: String,
      trim: true,
      unique: true,
      required: [true, "Enter Email Address"],
      validate: [
        (v: any) => {
          isEmail(v);
        },
        "Enter valid Email Address"
      ]
    },
    password: {
      type: String,
      required: [true, "Enter your password"]
    },
    profile: {
      fullname: {
        type: String,
        default: ""
      },
      dateofbirth: {
        type: Date
      },
      address: {
        type: String
      },
      city: {
        type: String
      },
      country: {
        type: String
      }
    }
  },

  { timestamps: true }
);

userSchema.pre("save", function(this: any, next) {
  if (this.password) {
    this.password = bcrypt.hashSync(this.password, 12);
  }
  next();
});

export default model("users", userSchema);
