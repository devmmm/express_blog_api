import { model, Schema } from "mongoose";
import mongoose from "mongoose";
const postSchema = new Schema(
  {
    title: {
      type: String,
      required: [true, "Required title"]
    },
    description: {
      type: String,
      required: [true, "Required description"]
    },
    user: {
      type: mongoose.Types.ObjectId,
      ref: "user"
    },
    category: {
      type: mongoose.Types.ObjectId,
      ref: "category"
    }
  },
  { timestamps: true }
);

export default model("post", postSchema);
