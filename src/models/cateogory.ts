import { model, Schema } from "mongoose";
import mongoose from "mongoose";
const categoryScehma = new Schema(
  {
    name: {
      type: String,
      required: [true, "Require category name"],
      unique: [true, "This category name is already exist!"]
    },
    user: {
      type: mongoose.Types.ObjectId,
      ref: "user"
    }
  },
  { timestamps: true }
);

export default model("category", categoryScehma);
