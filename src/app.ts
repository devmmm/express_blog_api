import express, { Request } from "express";
import dotenv from "dotenv";
import mongoose from "mongoose";
import UserRouter from "./routes/user";
import error from "./middlewares/error";
import PostRouter from "./routes/post";
import CategoryRouter from "./routes/category";
import multer, { FileFilterCallback } from "multer";
import { v4 } from "uuid";
import { rootDir } from "./util/upload";
import path from "path";
dotenv.config();
const port = process.env.PORT || "";
const db = process.env.DB || "";
const app = express();
const fileStorage = multer.diskStorage({
  destination: (_req, _file, cb) => {
    cb(null, "images");
  },
  filename: (_req, file, cb) => {
    cb(null, `${v4()}_${file.originalname}`);
  }
});

const filefilter = (_req: Request, file: any, cb: FileFilterCallback) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(
  multer({ storage: fileStorage, fileFilter: filefilter }).single("image")
);
app.use("/images", express.static(path.join(rootDir, "..", "images")));

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useFindAndModify: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .then(() => {
    app.listen(port, () => {
      console.log(`Server is running at port ${port}.`);
    });

    app.use(express.json());
    app.use("/api/v1/user/", UserRouter);
    app.use("/api/v1/post/", PostRouter);
    app.use("/api/v1/category/", CategoryRouter);
    app.use(error);

    //end of configuration
  })
  .catch(err => {
    console.log(err.message);
  });
