import { Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
export default function(req: any, res: Response, next: NextFunction) {
  const authHeader = req.get("Authorization");
  if (authHeader) {
    const token: any = authHeader.split(" ")[1];
    try {
      const decodedToken: any = jwt.verify(
        token,
        process.env.SECRET || "secret"
      );
      if (!decodedToken) {
        const error: any = new Error("Unauthenticated User!");
        error.statusCode = 401;
        throw error;
      } else {
        req.body.userId = decodedToken.userId;
        next();
      }
    } catch (error) {
      if (!error.statusCode) {
        error.statusCode = 401;
      }
      next(error);
    }
  } else {
    res.status(401).json({
      status: 0,
      message: "Unauthenticated User!"
    });
  }
}
