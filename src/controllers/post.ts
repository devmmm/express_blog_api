import { Request, Response, NextFunction } from "express";
import Post from "../models/post";
import { validationResult } from "express-validator";
// get all posts
export const allposts = async (
  _req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const posts = await Post.find({});
    if (posts) {
      res.status(200).json({
        Data: posts,
        status: 1,
        message: "success"
      });
    } else {
      const error: any = new Error("No articles found!");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

// get single post

export const getpost = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation Failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const post = await Post.findById(req.params.id);
    if (post) {
      res.status(200).json({
        Data: post,
        status: 1,
        message: "success"
      });
    } else {
      const error: any = new Error("Post not found");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

// create post

interface Mypost {
  title: String;
  description: String;
  user: any;
  category: any;
}

export const createpost = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation failed");
      error.statusCode = 400;
      error.data = errors.array();
      throw error;
    }
    const post_data: Mypost = {
      title: req.body.title,
      description: req.body.description,
      user: req.body.userId,
      category: req.body.categoryId
    };
    const post = new Post(post_data);
    post.save();
    if (post) {
      res.status(201).json({
        Data: post,
        status: 1,
        message: "Post created successfully!"
      });
    } else {
      const error: any = new Error("Post cannot create. ");
      error.statusCode = 400;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

// Edit Post
export const editpost = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation failed");
      error.statusCode = 400;
      error.data = errors.array();
      throw error;
    }
    const post: any = await Post.findById(req.body.id);
    if (post) {
      post.title = req.body.title;
      post.description = req.body.description;
      post.category = req.body.category;
      post.save();
      res.status(201).json({
        Data: post,
        status: 1,
        message: "Post updated successfully!"
      });
    } else {
      const error: any = new Error("Post does not exist");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
      throw error;
    }
    next(error);
  }
};

// Delete Post

export const deletepost = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation Failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const post = await Post.findByIdAndDelete(req.body.id);
    if (post) {
      res.status(200).json({
        Data: post,
        status: 1,
        message: "Post delete successfully"
      });
    } else {
      const error: any = new Error("Post does not exist!");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};
