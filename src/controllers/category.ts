import { Request, Response, NextFunction } from "express";
import Category from "../models/cateogory";
import { validationResult } from "express-validator";
// get all categories
export const getallcategory = async (
  _req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const categories = await Category.find();
    if (categories) {
      res.status(200).json({
        Data: categories,
        message: "success",
        status: 1
      });
    } else {
      const error: any = new Error("No categories found!");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};
// get category detail
export const categorydetail = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const category: any = await Category.findById(req.params.id);
    if (category) {
      res.status(200).json({
        Data: category,
        message: "success",
        status: 1
      });
    } else {
      const error: any = new Error("Category not found!");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};
// edit category
export const editcategory = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation Failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const category: any = Category.findById(req.body.id);
    if (category) {
      category.name = req.body.name;
      category.save();
      res.status(201).json({
        Data: category,
        status: 1,
        message: "Category updated successfully!"
      });
    } else {
      const error: any = new Error("Category not found!");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

// delete category
export const deletecategory = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation failed");
      error.statusCode = 400;
      error.data = errors.array();
      throw error;
    }
    const category = await Category.findByIdAndDelete(req.body.id);
    if (category) {
      res.status(200).json({
        Data: category,
        message: "Category delete successfully",
        status: 1
      });
    } else {
      const error: any = new Error("Category not found!");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

// create category
interface categoryData {
  name: String;
  id: any;
}

export const CreateCategory = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const categoryfind: any = await Category.findOne({ name: req.body.name });
    if (!categoryfind) {
      const create_data: categoryData = {
        name: req.body.name,
        id: req.body.userId
      };
      const category = new Category(create_data);
      category.save();
      if (category) {
        res.status(201).json({
          Data: category,
          message: "success",
          status: 1
        });
      } else {
        const error: any = new Error("Category create failed!");
        error.statusCode = 400;
        throw error;
      }
    } else {
      const error: any = new Error("Category name is already exist!");
      error.statusCode = 400;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};
