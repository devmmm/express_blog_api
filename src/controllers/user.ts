import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import bcrypt from "bcrypt";
import User from "../models/user";
import jwt from "jsonwebtoken";

interface userData {
  email: String;
  password: String;
}
export const signup = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const checkUser = await User.findOne({ email: req.body.email });
    if (checkUser) {
      const error: any = new Error("User email already exist!");
      error.statusCode = 400;
      throw error;
    } else {
      const createUser: userData = {
        email: req.body.email,
        password: req.body.password
      };
      const user = new User(createUser);
      user.save();
      if (user) {
        const token = jwt.sign(
          { userId: user.id },
          process.env.SECRET || "secret",
          { expiresIn: "1d" }
        );
        res.status(201).json({
          Data: user,
          access_token: token,
          status: 1,
          message: "User created successfully!"
        });
      } else {
        const error: any = new Error("User does not create!");
        error.statusCode = 422;
        throw error;
      }
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
      next(error);
    }
  }
};

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const checkUser: any = await User.findOne({ email: req.body.email });
    if (checkUser) {
      const checkPassword = bcrypt.compareSync(
        req.body.password,
        checkUser.password
      );
      if (checkPassword) {
        const token = jwt.sign(
          { userId: checkUser.id },
          process.env.SECRET || "secret",
          { expiresIn: "1d" }
        );
        res.status(201).json({
          Data: checkUser,
          access_token: token,
          status: 1,
          message: "User login successfully!"
        });
      } else {
        const error: any = new Error("Password wrong!");
        error.statusCode = 400;
        throw error;
      }
    } else {
      const error: any = new Error("User does not Exist!");
      error.statusCode = 400;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

export const Editprofile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const errors = validationResult(req);
  try {
    if (!errors.isEmpty()) {
      const error: any = new Error("Validation Failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    const user: any = await User.findById(req.body.userId);

    if (user) {
      user.profile.fullname = req.body.fullname || user.profile.fullname || "";
      user.profile.address = req.body.address || user.profile.address || "";
      user.profile.city = req.body.city || user.profile.city || "";
      user.profile.country = req.body.country || user.profile.country || "";
      user.save();
      res.status(201).json({
        Data: user,
        status: 1,
        message: "User updated successfully!"
      });
    } else {
      const error: any = new Error("User does not Exist!");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

export const getall_users = async (
  _req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const users = await User.find({});
    if (users) {
      res.status(200).json({
        Data: users,
        status: 1,
        message: "success"
      });
    } else {
      const error: any = new Error("No users");
      error.statusCode = 404;
      throw error;
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

export const edit_avatar = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error: any = new Error("validation failed");
      error.data = errors.array();
      error.statusCode = 400;
      throw error;
    }
    let avatar: any = req.file;
    // if ( avatar.mimetype !== "image/png" || "image/jpeg" || "image/jpg") {
    //   const error: any = new Error("file type does not support!");
    //   error.statusCode = 422;
    //   throw error;
    // }
    if (!avatar) {
      const error: any = new Error("No file picked");
      error.statusCode = 422;
      throw error;
    } else {
      const user: any = await User.findById(req.body.userId);
      if (user) {
        user.avatar = avatar.path;
        user.save();
        res.status(201).json({
          Data: user,
          message: "success",
          status: 1
        });
      } else {
        const error: any = new Error("User does not exist");
        error.statusCode = 404;
        throw error;
      }
    }
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};
