"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var dotenv_1 = __importDefault(require("dotenv"));
var mongoose_1 = __importDefault(require("mongoose"));
var user_1 = __importDefault(require("./routes/user"));
var error_1 = __importDefault(require("./middlewares/error"));
var post_1 = __importDefault(require("./routes/post"));
var category_1 = __importDefault(require("./routes/category"));
var multer_1 = __importDefault(require("multer"));
var uuid_1 = require("uuid");
var upload_1 = require("./util/upload");
var path_1 = __importDefault(require("path"));
dotenv_1.default.config();
var port = process.env.PORT || "";
var db = process.env.DB || "";
var app = express_1.default();
var fileStorage = multer_1.default.diskStorage({
    destination: function (_req, _file, cb) {
        cb(null, "images");
    },
    filename: function (_req, file, cb) {
        cb(null, uuid_1.v4() + "_" + file.originalname);
    }
});
var filefilter = function (_req, file, cb) {
    if (file.mimetype === "image/png" ||
        file.mimetype === "image/jpg" ||
        file.mimetype === "image/jpeg") {
        cb(null, true);
    }
    else {
        cb(null, false);
    }
};
app.use(multer_1.default({ storage: fileStorage, fileFilter: filefilter }).single("image"));
app.use("/images", express_1.default.static(path_1.default.join(upload_1.rootDir, "..", "images")));
mongoose_1.default
    .connect(db, {
    useNewUrlParser: true,
    useFindAndModify: true,
    useCreateIndex: true,
    useUnifiedTopology: true
})
    .then(function () {
    app.listen(port, function () {
        console.log("Server is running at port " + port + ".");
    });
    app.use(express_1.default.json());
    app.use("/api/v1/user/", user_1.default);
    app.use("/api/v1/post/", post_1.default);
    app.use("/api/v1/category/", category_1.default);
    app.use(error_1.default);
    //end of configuration
})
    .catch(function (err) {
    console.log(err.message);
});
//# sourceMappingURL=app.js.map