"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var isEmail_1 = __importDefault(require("validator/lib/isEmail"));
var bcrypt_1 = __importDefault(require("bcrypt"));
var userSchema = new mongoose_1.Schema({
    avatar: {
        type: String,
        default: ""
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        required: [true, "Enter Email Address"],
        validate: [
            function (v) {
                isEmail_1.default(v);
            },
            "Enter valid Email Address"
        ]
    },
    password: {
        type: String,
        required: [true, "Enter your password"]
    },
    profile: {
        fullname: {
            type: String,
            default: ""
        },
        dateofbirth: {
            type: Date
        },
        address: {
            type: String
        },
        city: {
            type: String
        },
        country: {
            type: String
        }
    }
}, { timestamps: true });
userSchema.pre("save", function (next) {
    if (this.password) {
        this.password = bcrypt_1.default.hashSync(this.password, 12);
    }
    next();
});
exports.default = mongoose_1.model("users", userSchema);
//# sourceMappingURL=user.js.map