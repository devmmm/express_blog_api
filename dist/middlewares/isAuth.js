"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function default_1(req, res, next) {
    var authHeader = req.get("Authorization");
    if (authHeader) {
        var token = authHeader.split(" ")[1];
        try {
            var decodedToken = jsonwebtoken_1.default.verify(token, process.env.SECRET || "secret");
            if (!decodedToken) {
                var error = new Error("Unauthenticated User!");
                error.statusCode = 401;
                throw error;
            }
            else {
                req.body.userId = decodedToken.userId;
                next();
            }
        }
        catch (error) {
            if (!error.statusCode) {
                error.statusCode = 401;
            }
            next(error);
        }
    }
    else {
        res.status(401).json({
            status: 0,
            message: "Unauthenticated User!"
        });
    }
}
exports.default = default_1;
//# sourceMappingURL=isAuth.js.map