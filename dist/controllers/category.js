"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateCategory = exports.deletecategory = exports.editcategory = exports.categorydetail = exports.getallcategory = void 0;
var cateogory_1 = __importDefault(require("../models/cateogory"));
var express_validator_1 = require("express-validator");
// get all categories
var getallcategory = function (_req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var categories, error, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, cateogory_1.default.find()];
            case 1:
                categories = _a.sent();
                if (categories) {
                    res.status(200).json({
                        Data: categories,
                        message: "success",
                        status: 1
                    });
                }
                else {
                    error = new Error("No categories found!");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                if (!error_1.statusCode) {
                    error_1.statusCode = 500;
                }
                next(error_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.getallcategory = getallcategory;
// get category detail
var categorydetail = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, category, error, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                return [4 /*yield*/, cateogory_1.default.findById(req.params.id)];
            case 2:
                category = _a.sent();
                if (category) {
                    res.status(200).json({
                        Data: category,
                        message: "success",
                        status: 1
                    });
                }
                else {
                    error = new Error("Category not found!");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_2 = _a.sent();
                if (!error_2.statusCode) {
                    error_2.statusCode = 500;
                }
                next(error_2);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.categorydetail = categorydetail;
// edit category
var editcategory = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, category, error;
    return __generator(this, function (_a) {
        errors = express_validator_1.validationResult(req);
        try {
            if (!errors.isEmpty()) {
                error = new Error("Validation Failed");
                error.data = errors.array();
                error.statusCode = 400;
                throw error;
            }
            category = cateogory_1.default.findById(req.body.id);
            if (category) {
                category.name = req.body.name;
                category.save();
                res.status(201).json({
                    Data: category,
                    status: 1,
                    message: "Category updated successfully!"
                });
            }
            else {
                error = new Error("Category not found!");
                error.statusCode = 404;
                throw error;
            }
        }
        catch (error) {
            if (!error.statusCode) {
                error.statusCode = 500;
            }
            next(error);
        }
        return [2 /*return*/];
    });
}); };
exports.editcategory = editcategory;
// delete category
var deletecategory = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, category, error, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation failed");
                    error.statusCode = 400;
                    error.data = errors.array();
                    throw error;
                }
                return [4 /*yield*/, cateogory_1.default.findByIdAndDelete(req.body.id)];
            case 2:
                category = _a.sent();
                if (category) {
                    res.status(200).json({
                        Data: category,
                        message: "Category delete successfully",
                        status: 1
                    });
                }
                else {
                    error = new Error("Category not found!");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                if (!error_3.statusCode) {
                    error_3.statusCode = 500;
                }
                next(error_3);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.deletecategory = deletecategory;
var CreateCategory = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, categoryfind, create_data, category, error, error, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                return [4 /*yield*/, cateogory_1.default.findOne({ name: req.body.name })];
            case 2:
                categoryfind = _a.sent();
                if (!categoryfind) {
                    create_data = {
                        name: req.body.name,
                        id: req.body.userId
                    };
                    category = new cateogory_1.default(create_data);
                    category.save();
                    if (category) {
                        res.status(201).json({
                            Data: category,
                            message: "success",
                            status: 1
                        });
                    }
                    else {
                        error = new Error("Category create failed!");
                        error.statusCode = 400;
                        throw error;
                    }
                }
                else {
                    error = new Error("Category name is already exist!");
                    error.statusCode = 400;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_4 = _a.sent();
                if (!error_4.statusCode) {
                    error_4.statusCode = 500;
                }
                next(error_4);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.CreateCategory = CreateCategory;
//# sourceMappingURL=category.js.map