"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.edit_avatar = exports.getall_users = exports.Editprofile = exports.login = exports.signup = void 0;
var express_validator_1 = require("express-validator");
var bcrypt_1 = __importDefault(require("bcrypt"));
var user_1 = __importDefault(require("../models/user"));
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var signup = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, checkUser, error, createUser, user, token, error, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                return [4 /*yield*/, user_1.default.findOne({ email: req.body.email })];
            case 2:
                checkUser = _a.sent();
                if (checkUser) {
                    error = new Error("User email already exist!");
                    error.statusCode = 400;
                    throw error;
                }
                else {
                    createUser = {
                        email: req.body.email,
                        password: req.body.password
                    };
                    user = new user_1.default(createUser);
                    user.save();
                    if (user) {
                        token = jsonwebtoken_1.default.sign({ userId: user.id }, process.env.SECRET || "secret", { expiresIn: "1d" });
                        res.status(201).json({
                            Data: user,
                            access_token: token,
                            status: 1,
                            message: "User created successfully!"
                        });
                    }
                    else {
                        error = new Error("User does not create!");
                        error.statusCode = 422;
                        throw error;
                    }
                }
                return [3 /*break*/, 4];
            case 3:
                error_1 = _a.sent();
                if (!error_1.statusCode) {
                    error_1.statusCode = 500;
                    next(error_1);
                }
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.signup = signup;
var login = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, checkUser, checkPassword, token, error, error, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                return [4 /*yield*/, user_1.default.findOne({ email: req.body.email })];
            case 2:
                checkUser = _a.sent();
                if (checkUser) {
                    checkPassword = bcrypt_1.default.compareSync(req.body.password, checkUser.password);
                    if (checkPassword) {
                        token = jsonwebtoken_1.default.sign({ userId: checkUser.id }, process.env.SECRET || "secret", { expiresIn: "1d" });
                        res.status(201).json({
                            Data: checkUser,
                            access_token: token,
                            status: 1,
                            message: "User login successfully!"
                        });
                    }
                    else {
                        error = new Error("Password wrong!");
                        error.statusCode = 400;
                        throw error;
                    }
                }
                else {
                    error = new Error("User does not Exist!");
                    error.statusCode = 400;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_2 = _a.sent();
                if (!error_2.statusCode) {
                    error_2.statusCode = 500;
                }
                next(error_2);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.login = login;
var Editprofile = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, user, error, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation Failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                return [4 /*yield*/, user_1.default.findById(req.body.userId)];
            case 2:
                user = _a.sent();
                if (user) {
                    user.profile.fullname = req.body.fullname || user.profile.fullname || "";
                    user.profile.address = req.body.address || user.profile.address || "";
                    user.profile.city = req.body.city || user.profile.city || "";
                    user.profile.country = req.body.country || user.profile.country || "";
                    user.save();
                    res.status(201).json({
                        Data: user,
                        status: 1,
                        message: "User updated successfully!"
                    });
                }
                else {
                    error = new Error("User does not Exist!");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                if (!error_3.statusCode) {
                    error_3.statusCode = 500;
                }
                next(error_3);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.Editprofile = Editprofile;
var getall_users = function (_req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var users, error, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, user_1.default.find({})];
            case 1:
                users = _a.sent();
                if (users) {
                    res.status(200).json({
                        Data: users,
                        status: 1,
                        message: "success"
                    });
                }
                else {
                    error = new Error("No users");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                if (!error_4.statusCode) {
                    error_4.statusCode = 500;
                }
                next(error_4);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.getall_users = getall_users;
var edit_avatar = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, avatar, error, user, error, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 4, , 5]);
                errors = express_validator_1.validationResult(req);
                if (!errors.isEmpty()) {
                    error = new Error("validation failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                avatar = req.file;
                if (!!avatar) return [3 /*break*/, 1];
                error = new Error("No file picked");
                error.statusCode = 422;
                throw error;
            case 1: return [4 /*yield*/, user_1.default.findById(req.body.userId)];
            case 2:
                user = _a.sent();
                if (user) {
                    user.avatar = avatar.path;
                    user.save();
                    res.status(201).json({
                        Data: user,
                        message: "success",
                        status: 1
                    });
                }
                else {
                    error = new Error("User does not exist");
                    error.statusCode = 404;
                    throw error;
                }
                _a.label = 3;
            case 3: return [3 /*break*/, 5];
            case 4:
                error_5 = _a.sent();
                if (!error_5.statusCode) {
                    error_5.statusCode = 500;
                }
                next(error_5);
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.edit_avatar = edit_avatar;
//# sourceMappingURL=user.js.map