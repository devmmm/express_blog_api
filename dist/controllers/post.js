"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deletepost = exports.editpost = exports.createpost = exports.getpost = exports.allposts = void 0;
var post_1 = __importDefault(require("../models/post"));
var express_validator_1 = require("express-validator");
// get all posts
var allposts = function (_req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var posts, error, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, post_1.default.find({})];
            case 1:
                posts = _a.sent();
                if (posts) {
                    res.status(200).json({
                        Data: posts,
                        status: 1,
                        message: "success"
                    });
                }
                else {
                    error = new Error("No articles found!");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                if (!error_1.statusCode) {
                    error_1.statusCode = 500;
                }
                next(error_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.allposts = allposts;
// get single post
var getpost = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, post, error, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation Failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                return [4 /*yield*/, post_1.default.findById(req.params.id)];
            case 2:
                post = _a.sent();
                if (post) {
                    res.status(200).json({
                        Data: post,
                        status: 1,
                        message: "success"
                    });
                }
                else {
                    error = new Error("Post not found");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_2 = _a.sent();
                if (!error_2.statusCode) {
                    error_2.statusCode = 500;
                }
                next(error_2);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.getpost = getpost;
var createpost = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, post_data, post, error;
    return __generator(this, function (_a) {
        errors = express_validator_1.validationResult(req);
        try {
            if (!errors.isEmpty()) {
                error = new Error("Validation failed");
                error.statusCode = 400;
                error.data = errors.array();
                throw error;
            }
            post_data = {
                title: req.body.title,
                description: req.body.description,
                user: req.body.userId,
                category: req.body.categoryId
            };
            post = new post_1.default(post_data);
            post.save();
            if (post) {
                res.status(201).json({
                    Data: post,
                    status: 1,
                    message: "Post created successfully!"
                });
            }
            else {
                error = new Error("Post cannot create. ");
                error.statusCode = 400;
                throw error;
            }
        }
        catch (error) {
            if (!error.statusCode) {
                error.statusCode = 500;
            }
            next(error);
        }
        return [2 /*return*/];
    });
}); };
exports.createpost = createpost;
// Edit Post
var editpost = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, post, error, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation failed");
                    error.statusCode = 400;
                    error.data = errors.array();
                    throw error;
                }
                return [4 /*yield*/, post_1.default.findById(req.body.id)];
            case 2:
                post = _a.sent();
                if (post) {
                    post.title = req.body.title;
                    post.description = req.body.description;
                    post.category = req.body.category;
                    post.save();
                    res.status(201).json({
                        Data: post,
                        status: 1,
                        message: "Post updated successfully!"
                    });
                }
                else {
                    error = new Error("Post does not exist");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                if (!error_3.statusCode) {
                    error_3.statusCode = 500;
                    throw error_3;
                }
                next(error_3);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.editpost = editpost;
// Delete Post
var deletepost = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var errors, error, post, error, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                errors = express_validator_1.validationResult(req);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                if (!errors.isEmpty()) {
                    error = new Error("Validation Failed");
                    error.data = errors.array();
                    error.statusCode = 400;
                    throw error;
                }
                return [4 /*yield*/, post_1.default.findByIdAndDelete(req.body.id)];
            case 2:
                post = _a.sent();
                if (post) {
                    res.status(200).json({
                        Data: post,
                        status: 1,
                        message: "Post delete successfully"
                    });
                }
                else {
                    error = new Error("Post does not exist!");
                    error.statusCode = 404;
                    throw error;
                }
                return [3 /*break*/, 4];
            case 3:
                error_4 = _a.sent();
                if (!error_4.statusCode) {
                    error_4.statusCode = 500;
                }
                next(error_4);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.deletepost = deletepost;
//# sourceMappingURL=post.js.map