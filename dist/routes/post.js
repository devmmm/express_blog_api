"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var post_1 = require("../controllers/post");
var express_validator_1 = require("express-validator");
var isAuth_1 = __importDefault(require("../middlewares/isAuth"));
var router = express_1.Router();
router.get("/allposts", [], post_1.allposts);
router.get("/post/:id", [], post_1.getpost);
router.post("/createpost", isAuth_1.default, [
    express_validator_1.body("title")
        .notEmpty()
        .withMessage("Required post title"),
    express_validator_1.body("description")
        .notEmpty()
        .withMessage("Required post description"),
    express_validator_1.body("category")
        .notEmpty()
        .withMessage("Category Id required.")
], post_1.createpost);
router.put("/editpost", isAuth_1.default, [
    express_validator_1.body("title")
        .notEmpty()
        .withMessage("Required post title"),
    express_validator_1.body("description")
        .notEmpty()
        .withMessage("Required post description"),
    express_validator_1.body("category")
        .notEmpty()
        .withMessage("Category Id required."),
    express_validator_1.body("id")
        .notEmpty()
        .withMessage("Post Id required.")
], post_1.editpost);
router.delete("/deletepost", isAuth_1.default, [
    express_validator_1.body("id")
        .notEmpty()
        .withMessage("Post Id required.")
], post_1.deletepost);
exports.default = router;
//# sourceMappingURL=post.js.map